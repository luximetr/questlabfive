﻿using Quest.Dto;
using System;
using System.Collections.Generic;

namespace Quest.Service
{
    
        public interface IDomainEntityService<TDto> where TDto : DomainEntityDto<TDto>
        {
            IList<Guid> ViewAll();

            TDto View(Guid domainId);
        }
   
}
