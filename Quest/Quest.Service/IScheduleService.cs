﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface IScheduleService : IDomainEntityService<ScheduleDto>
	{
		Guid CreateSchedule(Guid scriptId, DateTime timeBegin, TimeSpan duration);

		void AddTeam(Guid scheduleId, Guid teamId);

		void RemoveTeam(Guid scheduleId, Guid teamId);
	}
}
