﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface IPointService : IDomainEntityService<PointDto>
	{
		Guid CreatePoint(double X, double Y, string address, string hint);
	}
}
