﻿using System;
using Quest.Dto;

namespace Quest.Service
{
	public interface ICardService : IDomainEntityService<CardDto>
	{
		Guid CreateCard(Guid ownerId, string cardNumber, string bankName);
	}
}
