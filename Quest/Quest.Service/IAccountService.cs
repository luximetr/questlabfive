﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.Dto;

namespace Quest.Service
{
    public interface IAccountService : IDomainEntityService<AccountDto>
    {
        AccountDto Identify(string login, string password);  

        void ChangeLogin(Guid accountId, string newLogin);

        void ChangePassword(Guid accountId, string oldPassword, string newPassword);

		Guid CreateAdministrator(string login, string password);

		Guid CreateScriptwriter(string login, string password);

		Guid CreateUser(string login, string password, string name, string surname, string phone, string email);
    }
}
