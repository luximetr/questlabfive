﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quest.Model
{
	public class Team : Utils.Entity
	{

		public string Name { set; get; }
		private const int captainIndex = 0;

		public List<UserAccount> members { get; private set; }

		public Team(Guid domainId, string name, UserAccount captainAccount)
			: base(domainId)
		{
			Name = name;
			members = new List<UserAccount>();
			members.Add(captainAccount);
		}

		public void addTeamMember(UserAccount account)
		{
			if (!members.Contains(account))
			{
				members.Add(account);
			}			
		}

		public void editTeamName(string name, UserAccount captain)
		{
			if (captain.Equals(members[captainIndex]))
			{
				this.Name = name;
			}
			else
			{
				throw (new Exception("You have not permissino to rename team"));
			}
		}		

		public override string ToString()
		{
			StringBuilder resultString = new StringBuilder();
			resultString.AppendFormat("team '{0}'", Name);
			foreach(UserAccount account in members)
			{
				resultString.AppendFormat("\n  {0}", account);
			}
			return resultString.ToString();
		}
	}
}
