﻿using System;
using Quest.Model.Enums;

namespace Quest.Model
{
	public class Order : Utils.Entity
	{
		public decimal Cost { set; get; }
		public OrderStatus OrderStatus { set; get; }
		public Card Card { set; get; }
		public string ScriptTitle { set; get; }
		public Team Team { get; set; }

		public Order(Guid domainId, Script script, Team team, Card Card, Schedule schedule)
			: base(domainId)
		{
			this.Cost = script.Price;
			this.Card = Card;
			this.ScriptTitle = script.Title;
			this.Team = team;
			this.OrderStatus = OrderStatus.Placed;
			schedule.Teams.Add(team);
		}

		public void confirm()
		{
			OrderStatus = OrderStatus.Confirmed;
			Console.WriteLine("Order confirmed, total price: {0}", Cost);
		}

		public void cancel()
		{
			OrderStatus = OrderStatus.Canceled;
			Console.WriteLine("Order for {0} {1} was canceled", ScriptTitle, Team.Name);
		}

		public override string ToString()
		{
			return String.Format("{0} for {1} with price: {2}", ScriptTitle, Team.Name, Cost);
		}
	}
}
