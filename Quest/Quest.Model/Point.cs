﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class Point : Utils.Entity
	{
		public double X { set; get; }
		public double Y { set; get; }
		public string Address { set; get; }
		public string Hint { set; get; }

		public Point(Guid domainId, double x, double y, string address, string hint)
			: base(domainId)
		{
			X = x;
			Y = y;
			Address = address;
			Hint = hint;
		}

		public static bool operator ==(Point p1, Point p2)
		{
			return p1.X == p2.X && p1.Y == p2.Y;
		}

		public static bool operator !=(Point p1, Point p2)
		{
			return !(p1 == p2);
		}

		public override string ToString()
		{
			return String.Format("{0} {1} address: {2} hint: {3}", X, Y, Address, Hint);
		}
	}
}
