﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class UserAccount : Account
	{
		public string Name { set; get; }
		public string Surname { set; get; }
		public string Phone { set; get; }
		public string Email { set; get; }
		
		public UserAccount(Guid domainId, string login, string password, string name, string surname, string phone, string email)
			: base(domainId, login, password)
		{
			Name = name;
			Surname = surname;
			Phone = phone;
			Email = email;
		}

		public Team createTeam(string name)
		{
			return new Team(Guid.NewGuid(), name, this);
		}

		public Order createOrder(Script script, Team team, Card Card, Schedule schedule)
		{
			return new Order(Guid.NewGuid(), script, team, Card, schedule);
		}

		public void getHint()
		{
			double currentLocationX = 100.116;
			double currentLocationY = 102.122;
			//CurrentScript.getHint(currentLocationX, currentLocationY);
		}

		override public string ToString()
		{
			return String.Format("{0} {1} {2}", Name, Surname, Email);
		}
	}
}
