﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class ScriptwriterAccount : Account
	{

		public ScriptwriterAccount(Guid domainId, string login, string password)
			: base(domainId, login, password)
		{
		}

		public Script createScript(Guid domainId, string title, string description, decimal price)
		{
			return new Script(domainId, title, description, price);
		}

		public void addPointToScript(Script script, Point point)
		{
			if (script == null)
			{
				Console.WriteLine("No scripts found for scriptwriter");
			}
			if (point as object == null)
			{
				Console.WriteLine("Point for script must be not empty");
			}
			if (script != null && point as object != null)
			{
				script.addPointWithHint(point);
			}
		}

		public override string ToString()
		{
			return String.Format("login: {0}, password: {1}", Login, Password);
		}
	}
}
