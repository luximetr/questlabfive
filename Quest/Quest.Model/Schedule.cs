﻿using System;
using System.Collections.Generic;

namespace Quest.Model
{
	public class Schedule : Utils.Entity
	{
		public List<Team> Teams { private set; get; }
		public Script Script { set; get; }
		public DateTime TimeBegin { set; get; }
		public TimeSpan Duration { set; get; }

		public Schedule(Guid domainId, Script script, DateTime timeBegin, TimeSpan duration)
			: base(domainId)
		{
			this.Teams = new List<Team>();
			this.Script = script;
			this.TimeBegin = timeBegin;
			this.Duration = duration;
		}

		public void addTeam(Team team)
		{
			if (team != null)
			{
				if (!this.Teams.Contains(team))
				{
					this.Teams.Add(team);
				}
				else
				{
					throw new Exception(String.Format("Team {0} already registered for this script", team.Name));
				}
			}
			else
			{
				throw new Exception("Team can't be NULL");
			}
		}

		public void removeTeam(Team team)
		{
			this.Teams.Remove(team);
		}
	}
}
