﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class Account : Utils.Entity
	{
		public string Login { set; get; }
		public string Password { set; get; }

		public Account(Guid domainId, string login, string password)
			: base(domainId)
		{
			Login = login;
			Password = password;
		}

		public bool CheckPassword(string password)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			return this.Password == password;
		}
	}
}
