﻿using Quest.Service;
using Quest.Service.Impl;
using Quest.Repository;
using Quest.Repository.EntityFramework;

namespace Quest.TestApp
{
	class ServiceProvider
	{
		public ServiceProvider(QuestDbContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public IAccountService ProvideAccountService()
		{
			IAccountRepository accountRepository = RepositoryFactory.MakeAccountRepository(dbContext);

			return ServiceFactory.MakeAccountService(accountRepository);
		}

		public IOrderService ProvideOrderService()
		{
			IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository(dbContext);
			ITeamRepository teamRepository = RepositoryFactory.MakeTeamRepository(dbContext);
			IScriptRepository scriptRepository = RepositoryFactory.MakeScriptRepository(dbContext);
			ICardRepository cardRepository = RepositoryFactory.MakeCardRepository(dbContext);
			IScheduleRepository scheduleRepository = RepositoryFactory.MakeScheduleRepository(dbContext);

			return ServiceFactory.MakeOrderService(orderRepository, teamRepository, scriptRepository, cardRepository, scheduleRepository);
		}

		public IScheduleService ProvideScheduleService()
		{
			IScheduleRepository scheduleRepository = RepositoryFactory.MakeScheduleRepository(dbContext);
			ITeamRepository teamRepository = RepositoryFactory.MakeTeamRepository(dbContext);
			IScriptRepository scriptRepository = RepositoryFactory.MakeScriptRepository(dbContext);

			return ServiceFactory.MakeScheduleService(scheduleRepository, teamRepository, scriptRepository);

		}

		public IScriptService ProvideScriptService()
		{
			IScriptRepository scriptRepository = RepositoryFactory.MakeScriptRepository(dbContext);
			IPointRepository pointRepository = RepositoryFactory.MakePointRepository(dbContext);
			return ServiceFactory.MakeScriptService(scriptRepository, pointRepository);
		}

		public ITeamService ProvideTeamService()
		{
			ITeamRepository teamRepository = RepositoryFactory.MakeTeamRepository(dbContext);
			IAccountRepository accountRepository = RepositoryFactory.MakeAccountRepository(dbContext);

			return ServiceFactory.MakeTeamService(teamRepository, accountRepository);
		}

		public IPointService ProvidePointService()
		{
			IPointRepository pointRepository = RepositoryFactory.MakePointRepository(dbContext);

			return ServiceFactory.MakePointService(pointRepository);
		}

		public ICardService ProvideCardService()
		{
			ICardRepository cardRepository = RepositoryFactory.MakeCardRepository(dbContext);
			IAccountRepository accountRepository = RepositoryFactory.MakeAccountRepository(dbContext);

			return ServiceFactory.MakeCardService(cardRepository, accountRepository);
		}

		private QuestDbContext dbContext;
	}
}
