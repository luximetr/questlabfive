﻿using System.IO;
using System.Collections.Generic;
using Quest.Service;

namespace Quest.TestApp
{
	class ModelReporter
	{
		public ModelReporter(ServiceProvider serviceProvider, TextWriter output)
		{
			this.serviceProvider = serviceProvider;
			this.output = output;
		}

		public void GenerateReport()
		{
			ReportCollection("Accounts", serviceProvider.ProvideAccountService());
			ReportCollection("Orders", serviceProvider.ProvideOrderService());
			ReportCollection("Scripts", serviceProvider.ProvideScriptService());
			ReportCollection("Teams", serviceProvider.ProvideTeamService());
			ReportCollection("Schedules", serviceProvider.ProvideScheduleService());
			ReportCollection("Cards", serviceProvider.ProvideCardService());
			ReportCollection("Points", serviceProvider.ProvidePointService());
		}

		private void ReportCollection<TDto>(string title, IDomainEntityService<TDto> service)
			where TDto : Dto.DomainEntityDto<TDto>
		{
			output.WriteLine("==== {0} ==== ", title);
			output.WriteLine();

			foreach (var entityId in service.ViewAll())
			{
				output.Write(service.View(entityId));

				output.WriteLine();
				output.WriteLine();
			}

			output.WriteLine();
		}

		private ServiceProvider serviceProvider;
		private TextWriter output;
	}
}
