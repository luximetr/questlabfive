﻿using Quest.Service;
using System;

namespace Quest.TestApp
{
	class TestModelGenerator
	{
		public TestModelGenerator(ServiceProvider serviceProvider)
		{
			this.serviceProvider = serviceProvider;
		}

		public void GenerateTestData()
		{
			GenerateAccounts();
			GenerateTeams();
			GeneratePoints();
			GenerateScripts();
			GenerateCards();
			GenerateSchedules();
			GenerateOrders();
		}

		private void GenerateAccounts()
		{
			IAccountService service = serviceProvider.ProvideAccountService();

			Administrator = service.CreateAdministrator("admin", "adminPassword");
			Scriptwritter = service.CreateScriptwriter("scriptwriter1", "password");
			LexLutor = service.CreateUser("user1", "password1", "Lex", "Lutor", "5384165849", "lex@lex.usa");
			ClarcKent = service.CreateUser("user2", "password2", "Clarc", "Kent", "198761766", "supamen@dailyPlanet.usa");
			Captain = service.CreateUser("captain", "password", "Name", "Surname", "249620134", "captain@mail.ua");
			User1 = service.CreateUser("user3", "password", "Name", "Surname", "1346536", "user3@mail.ua");
			User2 = service.CreateUser("user4", "password", "Name", "Surname", "246923143", "user4@mail.ua");
		}

		private void GenerateOrders()
		{
			IOrderService service = serviceProvider.ProvideOrderService();

			Order1 = service.CreateOrder(LexLutor, BigAdventureSript, TeamMonkey, PrivateBankCard, Schedule);
			Order2 = service.CreateOrder(Captain, BigAdventureSript, TeamTigers, PrivateBankCard2, Schedule);
		}

		private void GeneratePoints()
		{
			IPointService service = serviceProvider.ProvidePointService();

			Point1 = service.CreatePoint(50, 50, "Pushkina, 20", "go to next point");
			Point2 = service.CreatePoint(52, 50, "Gogolya, 17", "go to next point");
			Point3 = service.CreatePoint(52, 52, "Nauki, 5", "grats, finish!");
		}

		private void GenerateScripts()
		{
			IScriptService service = serviceProvider.ProvideScriptService();

			BigAdventureSript = service.CreateScript("Big adventure", "good rest for you and your friend", 400);
			service.AddPoint(BigAdventureSript, Point1);
			service.AddPoint(BigAdventureSript, Point2);
			service.AddPoint(BigAdventureSript, Point3);
		}

		private void GenerateTeams()
		{
			ITeamService service = serviceProvider.ProvideTeamService();

			TeamMonkey = service.CreateTeam(LexLutor, "Monkey");
			service.AddTeamMember(TeamMonkey, ClarcKent);

			TeamTigers = service.CreateTeam(Captain, "Tigers");
			service.AddTeamMember(TeamTigers, User1);
			service.AddTeamMember(TeamTigers, User2);
		}

		private void GenerateCards()
		{
			ICardService service = serviceProvider.ProvideCardService();

			PrivateBankCard = service.CreateCard(LexLutor, "3601-2151-7647-2465", "Private Bank");
			PrivateBankCard2 = service.CreateCard(Captain, "3876-4359-1736-7359", "Private Bank");
		}

		private void GenerateSchedules()
		{
			IScheduleService service = serviceProvider.ProvideScheduleService();

			Schedule = service.CreateSchedule(BigAdventureSript, DateTime.Now + new TimeSpan(4, 0, 0, 0), new TimeSpan(5, 0, 0));
		}

		private Guid Point1, Point2, Point3;
		private Guid Order1, Order2;
		private Guid TeamMonkey, TeamTigers;
		private Guid BigAdventureSript;
		private Guid PrivateBankCard, PrivateBankCard2;
		private Guid LexLutor, ClarcKent, Captain, User1, User2;
		private Guid Administrator;
		private Guid Scriptwritter;
		private Guid Schedule;

		private ServiceProvider serviceProvider;
	}
}
