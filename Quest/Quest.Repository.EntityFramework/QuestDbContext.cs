﻿using System.Data.Entity;
using Quest.Model;

namespace Quest.Repository.EntityFramework
{
	public class QuestDbContext : DbContext
	{
		static QuestDbContext()
		{
			Database.SetInitializer(
				new DropCreateDatabaseAlways<QuestDbContext>()
			);
		}

		public QuestDbContext()
		{
			Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
		}

		public DbSet<Account> Accounts { get; set; }

		public DbSet<Card> Cards { get; set; }

		public DbSet<Order> Orders { get; set; }

		public DbSet<Point> Points { get; set; }

		public DbSet<Script> Scripts { get; set; }

		public DbSet<Team> Teams { get; set; }

		public DbSet<Schedule> Schedules { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new Configurations.AccountConfiguration());
			modelBuilder.Configurations.Add(new Configurations.AdministratorAccountConfiguration());
			modelBuilder.Configurations.Add(new Configurations.CardConfiguration());
			modelBuilder.Configurations.Add(new Configurations.OrderConfiguration());
			modelBuilder.Configurations.Add(new Configurations.PointConfiguration());
			modelBuilder.Configurations.Add(new Configurations.ScriptConfiguration());
			modelBuilder.Configurations.Add(new Configurations.ScriptwriterAccountConfiguration());
			modelBuilder.Configurations.Add(new Configurations.TeamConfiguration());
			modelBuilder.Configurations.Add(new Configurations.UserAccountConfiguration());
			modelBuilder.Configurations.Add(new Configurations.ScheduleConfiguration());
		}
	}
}
