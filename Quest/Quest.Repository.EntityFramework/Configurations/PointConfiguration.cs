﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class PointConfiguration : BasicEntityConfiguration<Point>
	{
		public PointConfiguration()
		{
			Property(p => p.X).IsRequired();
			Property(p => p.Y).IsRequired();
			Property(p => p.Address).IsRequired();
			Property(p => p.Hint).IsRequired();
		}
	}
}
