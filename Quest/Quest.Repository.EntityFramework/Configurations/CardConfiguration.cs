﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class CardConfiguration : BasicEntityConfiguration<Card>
	{
		public CardConfiguration()
		{
			Property(cc => cc.BankName).IsRequired();
			Property(cc => cc.CardNumber).IsRequired();
			Property(cc => cc.OwnerName).IsRequired();
		}
	}
}
