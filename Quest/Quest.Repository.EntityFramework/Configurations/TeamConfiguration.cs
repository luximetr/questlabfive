﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class TeamConfiguration : BasicEntityConfiguration<Team>
	{
		public TeamConfiguration()
		{
			Property(t => t.Name).IsRequired();
			HasMany<UserAccount>(t => t.members).WithOptional();
		}
	}
}
