﻿using System.Data.Entity.ModelConfiguration;

namespace Quest.Repository.EntityFramework.Configurations
{
	public abstract class BasicValueConfiguration<TValue> : ComplexTypeConfiguration<TValue>
		where TValue : class
	{
		protected BasicValueConfiguration()
		{
		}
	}
}
