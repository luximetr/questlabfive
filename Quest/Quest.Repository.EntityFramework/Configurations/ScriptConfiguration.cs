﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class ScriptConfiguration : BasicEntityConfiguration<Script>
	{
		public ScriptConfiguration()
		{
			Property(s => s.Title).IsRequired();
			Property(s => s.Description).IsRequired();
			Property(s => s.Price).IsRequired();
			Property(s => s.TimeConstraint).IsRequired();
			HasMany<Point>(s => s.Checkpoints).WithOptional();
		}
	}
}
