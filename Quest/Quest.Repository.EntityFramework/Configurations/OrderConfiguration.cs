﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class OrderConfiguration : BasicEntityConfiguration<Order>
	{
		public OrderConfiguration()
		{
			Property(o => o.Cost).IsRequired();
			HasRequired(o => o.Card);
			Property(o => o.ScriptTitle).IsRequired();
			HasRequired(o => o.Team);
		}
	}
}
