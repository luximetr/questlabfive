﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class ScheduleConfiguration : BasicEntityConfiguration<Schedule>
	{
		public ScheduleConfiguration()
		{
			Property(s => s.TimeBegin).IsRequired();
			Property(s => s.Duration).IsRequired();
			HasRequired(s => s.Script);
			HasMany<Team>(t => t.Teams).WithOptional();
		}
	}
}
