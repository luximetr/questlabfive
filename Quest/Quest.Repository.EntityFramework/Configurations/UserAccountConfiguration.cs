﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class UserAccountConfiguration : BasicEntityConfiguration<UserAccount>
	{
		public UserAccountConfiguration()
		{
			Property(ua => ua.Login).IsRequired();
			Property(ua => ua.Password).IsRequired();
			Property(ua => ua.Name).IsRequired();
			Property(ua => ua.Surname).IsRequired();
			Property(ua => ua.Phone).IsRequired();
			Property(ua => ua.Email).IsRequired();
		}
	}
}
