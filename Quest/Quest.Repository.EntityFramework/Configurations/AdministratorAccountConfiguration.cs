﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class AdministratorAccountConfiguration : BasicEntityConfiguration<AdministratorAccount>
	{
		public AdministratorAccountConfiguration()
		{
			Property(aa => aa.Login).IsRequired();
			Property(aa => aa.Password).IsRequired();
		}
	}
}
