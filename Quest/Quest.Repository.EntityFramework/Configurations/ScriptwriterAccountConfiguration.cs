﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.Configurations
{
	class ScriptwriterAccountConfiguration : BasicEntityConfiguration<ScriptwriterAccount>
	{
		public ScriptwriterAccountConfiguration()
		{
			Property(sa => sa.Login).IsRequired();
			Property(sa => sa.Password).IsRequired();
		}
	}
}
