﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class ScheduleRepository : BasicRepository<Schedule>, IScheduleRepository
	{
		public ScheduleRepository(QuestDbContext dbContext)
			: base(dbContext, dbContext.Schedules)
		{
		}
	}
}
