﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class CardRepository : BasicRepository<Card>, ICardRepository
	{
		public CardRepository(QuestDbContext dbContext)
			: base (dbContext, dbContext.Cards)
		{
		}
	}
}
