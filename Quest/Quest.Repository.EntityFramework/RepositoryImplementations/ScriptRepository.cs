﻿using Quest.Model;
using System.Linq;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class ScriptRepository : BasicRepository<Script>, IScriptRepository
	{
		public ScriptRepository(QuestDbContext dbContext)
			: base(dbContext, dbContext.Scripts)
		{
		}

		public Script FindByTitle(string title)
		{
			return GetDBSet().Where(s => s.Title == title).SingleOrDefault();
		}
	}
}
