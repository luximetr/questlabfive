﻿using System;
using System.Linq;
using System.Data.Entity;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public abstract class BasicRepository<T> where T : Quest.Utils.Entity
	{
		protected BasicRepository(QuestDbContext dbContext, DbSet<T> dbSet)
		{
			this.dbContext = dbContext;
			this.dbSet = dbSet;
		}

		protected QuestDbContext GetDBContext()
		{
			return this.dbContext;
		}

		protected DbSet<T> GetDBSet()
		{
			return this.dbSet;
		}

		public void Add(T obj)
		{
			dbSet.Add(obj);
		}

		public void Delete(T obj)
		{
			dbSet.Remove(obj);
		}

		public IQueryable<T> LoadAll()
		{
			return dbSet;
		}

		public T Load(int id)
		{
			return dbSet.Find(id);
		}

		public int Count()
		{
			return dbSet.Count();
		}

		public T FindByDomainId(Guid domainId)
		{
			return dbSet.Where(e => e.DomainId == domainId).SingleOrDefault();
		}


		public IQueryable<Guid> SelectAllDomainIds()
		{
			return dbSet.Select(e => e.DomainId);
		}

		public void StartTransaction()
		{
			this.dbContext.Database.BeginTransaction();
		}

		public void Commit()
		{
			this.dbContext.ChangeTracker.DetectChanges();
			this.dbContext.SaveChanges();
			this.dbContext.Database.CurrentTransaction.Commit();
		}

		public void Rollback()
		{
			this.dbContext.Database.CurrentTransaction.Rollback();
		}


		private QuestDbContext dbContext;
		private DbSet<T> dbSet;
	}
}
