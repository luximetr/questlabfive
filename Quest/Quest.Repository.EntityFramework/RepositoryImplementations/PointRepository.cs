﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class PointRepository : BasicRepository<Point>, IPointRepository
	{
		public PointRepository(QuestDbContext dbContext)
			: base (dbContext, dbContext.Points)
		{
		}
	}
}
