﻿using System.Linq;
using Quest.Model;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class AccountRepository : BasicRepository<Account>, IAccountRepository
	{
		public AccountRepository(QuestDbContext dbContext)
			: base(dbContext, dbContext.Accounts)
		{
		}

		public Account FindByLogin(string login)
		{
			return GetDBSet().Where(a => a.Login == login).SingleOrDefault();
		}
	}
}
