﻿using Quest.Model;

namespace Quest.Repository.EntityFramework.RepositoryImplementations
{
	public class OrderRepository : BasicRepository<Order>, IOrderRepository
	{
		public OrderRepository(QuestDbContext dbContext)
			: base (dbContext, dbContext.Orders)
		{
		}
	}
}
