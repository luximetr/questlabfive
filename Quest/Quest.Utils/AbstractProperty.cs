﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_2._0.Utils
{
	public abstract class AbstractProperty
	{
		public string ParamName { get; private set; }

		protected AbstractProperty(string paramName)
		{
			if (paramName == null)
				throw new ArgumentNullException("paramName");

			this.ParamName = paramName;
		}

	}
}
