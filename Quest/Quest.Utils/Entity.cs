﻿using System;

namespace Quest.Utils
{
	public abstract class Entity
	{
		public Guid DomainId { get; private set; }

		public long DatabaseId { get; set; }

		protected Entity() { }
		
		protected Entity(Guid domainId)
		{
			if (domainId == null)
				throw new ArgumentNullException("domainId");

			this.DomainId = domainId;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || GetType() != obj.GetType())
				return false;

			var otherEntity = (Entity)obj;
			return DomainId == otherEntity.DomainId;
		}

		public override int GetHashCode()
		{
			return DomainId.GetHashCode();
		}
	}
}
