﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Utils
{
	public abstract class Value<TValue>
		where TValue : Value<TValue>
	{
		public override bool Equals(object other)
		{
			return Equals(other as TValue);
		}

		public bool Equals(TValue other)
		{
			if (other == null)
			{
				return false;
			}

			var sequence1 = GetAttributesToIncludeInEqulityCheck();
			var sequence2 = other.GetAttributesToIncludeInEqulityCheck();
			return sequence1.SequenceEqual(sequence2);
		}

		public static bool operator ==(Value<TValue> left, Value<TValue> right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Value<TValue> left, Value<TValue> right)
		{
			return !(left == right);
		}

		public override int GetHashCode()
		{
			int hash = 17;
			foreach (var obj in this.GetAttributesToIncludeInEqulityCheck())
			{
				hash = hash * 31 + (obj == null ? 0 : obj.GetHashCode());
			}
			return hash;
		}

		protected abstract IEnumerable<object>
			GetAttributesToIncludeInEqulityCheck();
	}
}
