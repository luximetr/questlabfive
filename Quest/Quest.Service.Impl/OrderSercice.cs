﻿using System;
using Quest.Dto;
using Quest.Repository;
using Quest.Model;
using System.Linq;
using System.Collections.Generic;

namespace Quest.Service.Impl
{
	class OrderSercice : IOrderService
	{
		private IOrderRepository orderRepository;
		private ITeamRepository teamRepository;
		private IScriptRepository scriptRepository;
		private ICardRepository cardRepository;
		private IScheduleRepository scheduleRepository;

		public OrderSercice(IOrderRepository orderRepository, ITeamRepository teamRepository, IScriptRepository scriptRepository, ICardRepository cardRepository, IScheduleRepository scheduleRepository)
		{
			this.orderRepository = orderRepository;
			this.teamRepository = teamRepository;
			this.scriptRepository = scriptRepository;
			this.cardRepository = cardRepository;
			this.scheduleRepository = scheduleRepository;
		}

		public IList<Guid> ViewAll()
		{
			return orderRepository.SelectAllDomainIds().ToList();
		}

		public OrderDto View(Guid orderId)
		{
			Order order = ResolveOrder(orderId);
			return order.ToDto();
		}

		public Guid CreateOrder(Guid accountId, Guid scriptId, Guid teamId, Guid cardId, Guid scheduleId)
		{
			orderRepository.StartTransaction();
			Script script = ResolveScript(scriptId);
			Team team = ResolveTeam(teamId);
			Card card = ResolveCard(cardId);
			Schedule schedule = ResolveSchedule(scheduleId);

			Order order = new Order(Guid.NewGuid(), script, team, card, schedule);
			orderRepository.Add(order);
			orderRepository.Commit();
			return order.DomainId;
		}

		public void ConfirmOrder(Guid orderId)
		{
			orderRepository.StartTransaction();
			Order order = ResolveOrder(orderId);
			order.OrderStatus = Model.Enums.OrderStatus.Confirmed;
			orderRepository.Commit();
		}

		public void CancelOrder(Guid orderId)
		{
			orderRepository.StartTransaction();
			Order order = ResolveOrder(orderId);
			order.OrderStatus = Model.Enums.OrderStatus.Canceled;
			orderRepository.Commit();
		}

		private Order ResolveOrder(Guid orderId)
		{
			return ServiceUtils.ResolveEntity(orderRepository, orderId);
		}

		private Script ResolveScript(Guid scriptId)
		{
			return ServiceUtils.ResolveEntity(scriptRepository, scriptId);
		}

		private Team ResolveTeam(Guid teamId)
		{
			return ServiceUtils.ResolveEntity(teamRepository, teamId);
		}

		private Card ResolveCard(Guid cardId)
		{
			return ServiceUtils.ResolveEntity(cardRepository, cardId);
		}

		private Schedule ResolveSchedule(Guid scheduleId)
		{
			return ServiceUtils.ResolveEntity(scheduleRepository, scheduleId);
		}
	}
}
