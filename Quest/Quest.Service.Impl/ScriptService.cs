﻿using System;
using Quest.Dto;
using Quest.Repository;
using Quest.Model;
using System.Collections.Generic;
using System.Linq;

namespace Quest.Service.Impl
{
	class ScriptService : IScriptService
	{
		private IScriptRepository scriptRepository;
		private IPointRepository pointRepository;

		public IList<Guid> ViewAll()
		{
			return scriptRepository.SelectAllDomainIds().ToList();
		}

		public ScriptDto View(Guid scriptId)
		{
			Script script = ResolveScript(scriptId);
			return script.ToDto();
		}

		public ScriptService(IScriptRepository scriptRepository, IPointRepository pointRepository)
		{
			this.scriptRepository = scriptRepository;
			this.pointRepository = pointRepository;
		}

		public Guid CreateScript(string title, string description, decimal price)
		{
			scriptRepository.StartTransaction();
			Script script = scriptRepository.FindByTitle(title);
			if (script != null)
			{
				throw new ArgumentException("Duplicate script");
			}
			scriptRepository.StartTransaction();

			Script newScript = new Script(Guid.NewGuid(), title, description, price);
			scriptRepository.Add(newScript);
			scriptRepository.Commit();
			return newScript.DomainId;
		}

		public void AddPoint(Guid scriptId, Guid pointId)
		{
			scriptRepository.StartTransaction();
			Script script = ResolveScript(scriptId);
			Point point = ResolvePoint(pointId);
			script.addPointWithHint(point);
			scriptRepository.Commit();
		}

		public string GetHint(Guid scriptId, double x, double y)
		{
			scriptRepository.StartTransaction();
			Script script = ResolveScript(scriptId);
			string hint = script.getHint(x, y);
			scriptRepository.Commit();
			return hint;
		}

		public int GetPointsCount(Guid scriptId)
		{
			scriptRepository.StartTransaction();
			Script script = ResolveScript(scriptId);
			int hintCount = script.getCheckpointsCount();
			scriptRepository.Commit();
			return hintCount;
		}

		private Script ResolveScript(Guid scriptId)
		{
			return ServiceUtils.ResolveEntity(scriptRepository, scriptId);
		}

		private Point ResolvePoint(Guid pointId)
		{
			return ServiceUtils.ResolveEntity(pointRepository, pointId);
		}
	}
}
