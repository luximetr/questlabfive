﻿using Quest.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Quest.Dto;
using Quest.Model;

namespace Quest.Service.Impl
{
	public class CardService : ICardService
	{
		private ICardRepository cardRepository;
		private IAccountRepository accountRepository;

		public CardService(ICardRepository cardRepository, IAccountRepository accountRepository)
		{
			this.cardRepository = cardRepository;
			this.accountRepository = accountRepository;
		}

		public Guid CreateCard(Guid ownerId, string cardNumber, string bankName)
		{
			cardRepository.StartTransaction();
			UserAccount account = (UserAccount)ResolveAccount(ownerId);
			Card card = new Card(Guid.NewGuid(), account.Name, cardNumber, bankName);
			cardRepository.Add(card);
			cardRepository.Commit();
			return card.DomainId;
		}

		public CardDto View(Guid cardId)
		{
			Card card = ResolveCard(cardId);
			return card.ToDto();
		}

		public IList<Guid> ViewAll()
		{
			return cardRepository.SelectAllDomainIds().ToList();
		}

		private Card ResolveCard(Guid cardId)
		{
			return ServiceUtils.ResolveEntity(cardRepository, cardId);
		}

		private Account ResolveAccount(Guid accountId)
		{
			return ServiceUtils.ResolveEntity(accountRepository, accountId);
		}
	}
}
