﻿using Quest.Repository;

namespace Quest.Service.Impl
{
	public static class ServiceFactory
	{
		public static IAccountService MakeAccountService(IAccountRepository accountRepository)
		{
			return new AccountService (accountRepository);
		}

		public static IOrderService MakeOrderService(
			IOrderRepository orderRepository, 
			ITeamRepository teamRepository,
			IScriptRepository scriptRepository,
			ICardRepository cardRepository,
			IScheduleRepository scheduleRepository)
		{
			return new OrderSercice(
				orderRepository,
				teamRepository,
				scriptRepository,
				cardRepository,
				scheduleRepository
				);
		}

		public static IScheduleService MakeScheduleService(IScheduleRepository scheduleRepository, ITeamRepository teamRepository, IScriptRepository scriptRepository)
		{
			return new ScheduleService(scheduleRepository, teamRepository, scriptRepository);
		}

		public static IScriptService MakeScriptService(IScriptRepository scriptRepository, IPointRepository pointRepository)
		{
			return new ScriptService(scriptRepository, pointRepository);
		}

		public static ITeamService MakeTeamService(ITeamRepository teamRepository, IAccountRepository accountRepository)
		{
			return new TeamService(teamRepository, accountRepository);
		}

		public static IPointService MakePointService(IPointRepository repository)
		{
			return new PointService(repository);
		}

		public static ICardService MakeCardService(ICardRepository cardRepository, IAccountRepository accountRepository)
		{
			return new CardService(cardRepository, accountRepository);
		}
	}
}
