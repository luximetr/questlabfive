﻿using System;
using System.Collections.Generic;
using Quest.Dto;
using Quest.Repository;
using Quest.Model;
using System.Linq;

namespace Quest.Service.Impl
{
	class PointService : IPointService
	{
		private IPointRepository pointRepository;

		public PointService(IPointRepository pointRepository)
		{
			this.pointRepository = pointRepository;
		}
		
		public Guid CreatePoint(double X, double Y, string address, string hint)
		{
			pointRepository.StartTransaction();
			Point point = new Point(Guid.NewGuid(), X, Y, address, hint);
			pointRepository.Add(point);
			pointRepository.Commit();
			return point.DomainId;
		}

		public PointDto View(Guid pointId)
		{
			Point point = ResolvePoint(pointId);
			return point.ToDto();
		}

		public IList<Guid> ViewAll()
		{
			return pointRepository.SelectAllDomainIds().ToList();
		}

		private Point ResolvePoint(Guid pointId)
		{
			return ServiceUtils.ResolveEntity(pointRepository, pointId);
		}
	}
}
