﻿using System;
using Quest.Repository;

namespace Quest.Service.Impl
{
	sealed class ServiceUtils
	{
		private ServiceUtils() { }

		public static TEntity ResolveEntity<TEntity>(
			IRepository<TEntity> repository,
			Guid domainId
		) where TEntity : Utils.Entity
		{
			TEntity entity = repository.FindByDomainId(domainId);
			if (entity != null)
				return entity;

			throw new ArgumentException("Unresolved entity");
		}
	}
}
