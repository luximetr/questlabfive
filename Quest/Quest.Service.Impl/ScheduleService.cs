﻿using System;
using Quest.Dto;
using Quest.Repository;
using Quest.Model;
using System.Collections.Generic;
using System.Linq;

namespace Quest.Service.Impl
{
	class ScheduleService : IScheduleService
	{
		private IScheduleRepository scheduleRepository;
		private ITeamRepository teamRepository;
		private IScriptRepository scriptRepository;

		public ScheduleService(IScheduleRepository scheduleRepository, ITeamRepository teamRepository, IScriptRepository scriptRepository)
		{
			this.scheduleRepository = scheduleRepository;
			this.teamRepository = teamRepository;
            this.scriptRepository = scriptRepository;
		}

		public Guid CreateSchedule(Guid scriptId, DateTime timeBegin, TimeSpan duration)
		{
			scheduleRepository.StartTransaction();
			Script script = ResolveScript(scriptId);
			Schedule schedule = new Schedule(Guid.NewGuid(), script, timeBegin, duration);
			scheduleRepository.Add(schedule);
			scheduleRepository.Commit();
			return schedule.DomainId;
		}

		public IList<Guid> ViewAll()
		{
			return scheduleRepository.SelectAllDomainIds().ToList();
		}

		public ScheduleDto View(Guid scheduleId)
		{
			Schedule schedule = ResolveSchedule(scheduleId);
			return schedule.ToDto();
		}

		public void AddTeam(Guid scheduleId, Guid teamId)
		{
			teamRepository.StartTransaction();
			Team team = ResolveTeam(teamId);
			Schedule schedule = ResolveSchedule(scheduleId);
			schedule.addTeam(team);
			scheduleRepository.Commit();
		}

		public void RemoveTeam(Guid scheduleId, Guid teamId)
		{
			teamRepository.StartTransaction();
			Team team = ResolveTeam(teamId);
			Schedule schedule = ResolveSchedule(scheduleId);
			schedule.removeTeam(team);
			scheduleRepository.Commit();
		}

		private Team ResolveTeam(Guid teamId)
		{
			return ServiceUtils.ResolveEntity(teamRepository, teamId);
		}

		private Schedule ResolveSchedule(Guid scheduleId)
		{
			return ServiceUtils.ResolveEntity(scheduleRepository, scheduleId);
		}

		private Script ResolveScript(Guid scriptId)
		{
			return ServiceUtils.ResolveEntity(scriptRepository, scriptId);
		}
	}
}
