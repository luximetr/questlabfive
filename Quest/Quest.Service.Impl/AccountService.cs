﻿using System;
using System.Collections.Generic;
using System.Linq;
using Quest.Dto;
using Quest.Repository;
using Quest.Model;

namespace Quest.Service.Impl
{
    public class AccountService : IAccountService
    {
		private IAccountRepository accountRepository;

		public AccountService(IAccountRepository accountRepository)
		{
			this.accountRepository = accountRepository;
		}

		public IList<Guid> ViewAll()
		{
			return accountRepository.SelectAllDomainIds().ToList();
		}

		public AccountDto View(Guid accountId)
		{
			Account account = ResolveAccount(accountId);
			return account.ToDto();
		}

		public AccountDto Identify(string login, string password)
		{
			Account account = accountRepository.FindByLogin(login);
			if (account == null)
			{
				return null;
			}
			if (!account.CheckPassword(password))
			{
				return null;
			}
			return account.ToDto();
		}

		public void ChangeLogin(Guid accountId, string newLogin)
		{
			accountRepository.StartTransaction();

			Account account = ResolveAccount(accountId);
			if (account != null)
			{
				throw new ArgumentException("Duplicate account");
			}
			account.Login = newLogin;
			accountRepository.Commit();
		}

		public void ChangePassword(Guid accountId, string oldPassword, string newPassword)
		{
			accountRepository.StartTransaction();

			Account account = ResolveAccount(accountId);

			if (!account.CheckPassword(oldPassword))
				throw new ArgumentException("Password failed");

			account.Password = newPassword;

			accountRepository.Commit();
		}

		public Guid CreateAdministrator(string login, string password)
		{
			Account account = accountRepository.FindByLogin(login);
			if (account != null)
			{
				throw new ArgumentException("Duplicate account");
			}
			accountRepository.StartTransaction();

			AdministratorAccount administratorAccount = new AdministratorAccount(Guid.NewGuid(), login, password);
			accountRepository.Add(administratorAccount);
			accountRepository.Commit();
			return administratorAccount.DomainId;
		}

		public Guid CreateScriptwriter(string login, string password)
		{
			Account account = accountRepository.FindByLogin(login);
			if (account != null)
			{
				throw new ArgumentException("Duplicate account");
			}
			accountRepository.StartTransaction();

			ScriptwriterAccount scriptwriterAccount = new ScriptwriterAccount(Guid.NewGuid(), login, password);
			accountRepository.Add(scriptwriterAccount);
			accountRepository.Commit();
			return scriptwriterAccount.DomainId;
		}

		public Guid CreateUser(string login, string password, string name, string surname, string phone, string email)
		{
			Account account = accountRepository.FindByLogin(login);
			if (account != null)
			{
				throw new ArgumentException("Duplicate account");
			}
			accountRepository.StartTransaction();

			UserAccount userAccount = new UserAccount(Guid.NewGuid(), login, password, name, surname, phone, email);
			accountRepository.Add(userAccount);
			accountRepository.Commit();
			return userAccount.DomainId;
		}

		private Account ResolveAccount(Guid accountId)
		{
			return ServiceUtils.ResolveEntity(accountRepository, accountId);
		}
	}
}
