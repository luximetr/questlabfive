﻿using Quest.Model;

namespace Quest.Repository
{
	public interface ITeamRepository : IRepository<Team>
	{
		Team FindByName(string name);
	}
}
