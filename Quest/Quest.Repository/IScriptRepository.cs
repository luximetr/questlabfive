﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IScriptRepository : IRepository<Script>
	{
		Script FindByTitle(string title);
	}
}
