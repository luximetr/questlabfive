﻿using System;
using System.Collections.Generic;
using System.Linq;
using Quest.Utils;

namespace Quest.Repository
{
	public interface IRepository< T > where T : Utils.Entity
	{
		void StartTransaction();

		void Commit();

		void Rollback();

		int Count();

		T Load(int id);

		IQueryable<T> LoadAll();

		void Add(T t);

		void Delete(T t);

		T FindByDomainId(Guid domainId);

		IQueryable<Guid> SelectAllDomainIds();
	}
}
