﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IPointRepository : IRepository<Point>
	{
	}
}
