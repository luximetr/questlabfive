﻿using Quest.Model;

namespace Quest.Repository
{
	public interface ICardRepository : IRepository<Card>
	{
	}
}
