﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IOrderRepository : IRepository<Order>
	{
	}
}
