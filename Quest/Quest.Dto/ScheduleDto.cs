﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class ScheduleDto:DomainEntityDto<ScheduleDto>
    {
        public IList<TeamDto> Teams { private set; get; }
        public Guid ScriptId { private set; get; }
        public DateTime TimeBegin { private set; get; }
        public TimeSpan Duration { private set; get; }

        public ScheduleDto(Guid domainId, Guid scriptId, DateTime timeBegin, TimeSpan duration)
            :base(domainId)
        {
            this.Teams = new List<TeamDto>();
            this.ScriptId = scriptId;
            this.TimeBegin = timeBegin;
            this.Duration = duration;
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            var list = new List<object>() { DomainId, ScriptId, TimeBegin, Duration };
			foreach (var team in Teams)
			{
				list.Add(team);
			}
			return list;
        }

        public override string ToString()
        {
            return String.Format("DomainId = {0}\nTimeBegin = {1}\nDuration{2}\n ",DomainId,TimeBegin,Duration);
        }


    }
}
