﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quest.Dto
{
   public class TeamDto : DomainEntityDto<TeamDto>
    {
        public string Name {private set; get; }
        private const int captainIndex = 0;

        public IList<AccountDto> members { get; private set; }

        public TeamDto(Guid domainId, string name, AccountDto captainAccount)
			: base(domainId)
		{
            Name = name;
            members = new List<AccountDto>();
            members.Add(captainAccount);
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            var list = new List<object>() { DomainId, Name};
			foreach (var member in members)
			{
				list.Add(member);
			}
			return list;
        }

        public override string ToString()
        {
            StringBuilder resultString = new StringBuilder();
            resultString.AppendFormat("team '{0}'", Name);
            foreach (var account in members)
            {
                resultString.AppendFormat("\n  {0}", account);
            }
            return resultString.ToString();
        }
    }
}
