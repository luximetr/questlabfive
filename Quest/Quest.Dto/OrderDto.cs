﻿using System;
using System.Collections.Generic;

namespace Quest.Dto
{
    public class OrderDto : DomainEntityDto<OrderDto>
    {
        public decimal Cost { private set; get; }  

        public string OrderStatus {private set; get; }

        public Guid CardId { private set; get; }

        public string ScriptTitle {private set; get; }

        public Guid TeamId { get; private set; }

        public OrderDto(Guid domainId, decimal cost, Guid teamId,string orderstatus, Guid cardId, string scriptTitle)
            :base(domainId)
		{
            this.Cost = cost;
            this.CardId = cardId;
            this.ScriptTitle = scriptTitle;
            this.TeamId = teamId;
            this.OrderStatus = orderstatus;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqulityCheck()
        {
            return new List<object> { DomainId, Cost, CardId, ScriptTitle, TeamId, OrderStatus };
        }

        public override string ToString()
        {
            return string.Format("DomainId ={0}, Cost = {1}\nPaymentMethod = {2}\n ScriptTitle = {3}\nOrderStatus = {4}\n ", DomainId, Cost, ScriptTitle, OrderStatus);
        }
    }
}
